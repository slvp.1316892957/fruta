//liberia reactiva rxjs
const {from, of } = require("rxjs");
const {delay, concatMap } = require ("rxjs/operators");

const frutas = ["pera", "sandia", "manzana", "naranja"];

//observable
const frutasObs = from(frutas).pipe(
    concatMap(item => of(item).pipe(delay(1000))) //1S
);

const picarFruta = (fruta) => {
    return `${fruta} picada`;
};

const frutasPicadasObs = frutasObs.pipe(map(picarFruta));

frutasObs.subscribe(fruta =>{
    console.log(`Me llego una ${fruta}`);
});