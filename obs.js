const {from, of } = require("rxjs");
const {delay, concatMap } = require ("rxjs/operators");

const frutas = ["pera", "sandia", "manzana", "naranja"];

//observable
const frutasObs = from(frutas).pipe(
    concatMap(item => of(item).pipe(delay(1000))) //1S
);

frutasObs.subscribe(fruta =>{
    console.log(`Me llego una ${fruta}`);
});

console.log("Ya realice el pedido");