const {from, of, zip } = require("rxjs");
const {delay, concatMap } = require ("rxjs/operators");

const frutas = ["pera", "sandia", "manzana", "naranja"];
const verdura = ["Lechuga", "acelga", "apio", "espinaca"];

//observable 1
const frutasObs = from(frutas).pipe(
    concatMap(item => of(item).pipe(delay(1000))) //1S
);

//observable 2
const verdurasObs = from(verduras);

//observable 3 la combinacion
const frutasyVerduras = zip (frutasObs, verdurasObs);

frutasyVerduras.subscribe(frutasyVerdura =>{
    console.log(`Me llego una ${frutasyVerdura}`);
});